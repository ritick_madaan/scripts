export const ignoredApps = [
    "in.juspay.gatekeeper",
    "in.juspay.gpay",
    "in.juspay.hypermanage",
    "in.juspay.upimanage",
    "in.juspay.upi",
    "in.juspay.becknuser",
    "net.openkochi.yatri",
    "in.juspay.becknui",
    "in.juspay.beckn.transporter",
    "net.openkochi.yatripartner",
    "in.juspay.sahay",
    "in.juspay.arya",
    "in.hyper.pay",
    "in.sahay.gem",
    "in.sahay.gst"
  ];