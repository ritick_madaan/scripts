import { ignoredApps } from "./constants.mjs";
import { config, withStagger } from "./inputs.mjs";
import clipboardy from "clipboardy";

// Generator

const MJOSConfig = {
  live: {
    package_version: "1.0",
    package: {},
    assets: {},
  },
  dependencies: {},
  app_list: [],
};

for (var app in config.apps) {
  if (ignoredApps.includes(app)) {
    continue;
  }
  for (var asset in config.apps[app].assets) {
    const val = config.apps[app].assets[asset];
    if (asset === "index_bundle") {
      MJOSConfig.live.package[app] = val;
    } else {
      if (!MJOSConfig.live.assets[app]) {
        MJOSConfig.live.assets[app] = {};
      }
      MJOSConfig.live.assets[app][asset] = val;
    }
  }
  MJOSConfig.dependencies[app] = { default: {} };
  MJOSConfig.dependencies[app].default["root"] = config.apps[app].root;
  MJOSConfig.dependencies[app].default["entry"] = config.apps[app].entry;
  const canOpen = config.apps[app].canOpen;
  if (canOpen.length != 0) {
    MJOSConfig.dependencies[app].default["required_apps"] = canOpen;
  }
  MJOSConfig.app_list.push(app);
}

if (withStagger) {
  MJOSConfig.stagger = 0;
  MJOSConfig.new = JSON.parse(JSON.stringify(MJOSConfig.live));

  for (var app in config.apps) {
    if (ignoredApps.includes(app)) {
      continue;
    }

    for (var asset in config.apps[app].newAssets) {
      const val = config.apps[app].newAssets[asset];
      if (asset === "index_bundle") {
        MJOSConfig.new.package[app] = val;
      } else {
        MJOSConfig.new.assets[app][asset] = val;
      }
    }
  }

  MJOSConfig.new.package_version = "1.1";
}

// Copy
clipboardy.writeSync(JSON.stringify(MJOSConfig));
