const staggerData = {
  juspay: {
    payments: {
      "in.juspay.dotp": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 25 } } },
          "v1-config.zip": {},
          "2.0rc1": { "v1-index_bundle.zip": { common: 25 } },
        },
      },
      "2.0": {
        release: {
          "v1-polyfill.zip": {},
          "v1-tracker.zip": {},
          "v1-config.zip": {},
          "polyfill.js": { bb: 99, bboutdoor: 99 },
          "tracker.js": { common: 20 },
          "staggered-release-assets": { "tracker.js": { common: 50 } },
        },
      },
      "in.juspay.dotp.bms": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} } },
        beta: { "2.0rc1": { "v1-index_bundle.zip": { common: 1 } } },
      },
      "in.juspay.dotp.zee5": { beta: { "2.0rc1": { "v1-index_bundle.zip": { common: 2 } } } },
      "in.juspay.ec.bboutdoor": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "staggered-release-assets": { "v1-index_bundle.zip": { common: 10 } },
          "v1-index_bundle.zip": { common: 99 },
        },
      },
      "in.juspay.ec.slice": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.ec.grofers": { release: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      "in.juspay.flyer.timesprime": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.flyer.dineout": {
        release: {
          IOS: { "2.0rc1": { "index_bundle.js": { common: 99 }, "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": {}, "index_bundle.js": { common: 99 } },
        },
      },
      "in.juspay.ec": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.jsa": {}, "index_bundle.js": {}, "v1-index_bundle.zip": {} } },
          "2.0rc1": { "index_bundle.js": {}, "v1-index_bundle.zip": {} },
          "v1-config.zip": { common: 99 },
          "config.js": { common: 99 },
          "v1-polyfill.zip": {},
        },
        beta: { "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.flyer.ixigoprod": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.ec.olacabs": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.flyer": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": {}, "index_bundle.js": { meesho: 99 } },
        },
      },
      "in.juspay.escrow": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.godel": {
        release: {
          "1.0rc2": { "v1-acs.zip": {} },
          "2.0rc1": { "index_bundle.js": {}, "v1-index_bundle.zip": { "1mg": 99 } },
        },
      },
      "in.juspay.godel.tatapay": { release: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      "in.juspay.hyperpay.a23games": {
        release: {
          IOS: {
            "v1-icons.zip": { common: 99 },
            "v1-configuration.zip": {},
            "v1-strings.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          },
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          "v1-strings.zip": {},
          "v1-icons.zip": { common: 99 },
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.acko": {
        release: {
          "2.0rc1": {
            "v1-icons.zip": {},
            "v1-configuration.zip": {},
            "v1-strings.zip": {},
            "v1-index_bundle.zip": {},
          },
          IOS: {
            "2.0rc1": { "v1-index_bundle.zip": {}, "v1-configuration.zip": {} },
            "v1-configuration.zip": {},
            "v1-icons.zip": {},
            "v1-strings.zip": {},
          },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.ec.dineout": {
        release: {
          IOS: { "2.0rc1": { "index_bundle.js": { common: 99 }, "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": {} },
        },
      },
      "in.juspay.ec.swiggy": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.escrow.acko": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.hyperpay.adda52poker": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.ackostable": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.hyperpay.astrostable": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } } },
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
        },
      },
      "in.juspay.hyperpay.all": { release: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      "in.juspay.hyperpay.astroyogi": {
        release: {
          "v1-icons.zip": {},
          IOS: {
            "v1-strings.zip": {},
            "v1-configuration.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": { common: 50 } },
            "v1-icons.zip": {},
          },
          "v1-strings.zip": {},
          "2.0rc1": { "v1-index_bundle.zip": { common: 50 } },
          "v1-configuration.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.bbfresho": {
        release: { "v1-strings.zip": {}, "2.0rc1": { "v1-index_bundle.zip": {} }, "v1-ui_config.zip": {} },
      },
      "in.juspay.hyperpay.bboutdoor": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          "staggered-release-assets": { "ui_config.js": {} },
          "v1-ui_config.zip": {},
          "ui_config.js": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.bbinstant": {
        release: {
          "v1-configuration.zip": {},
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {} },
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.citymall": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-strings.zip": {},
          "v1-configuration.zip": { common: 99 },
        },
      },
      "in.juspay.hyperpay.cars24": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 60 } }, "v1-configuration.zip": {} },
          "v1-configuration.zip": {},
          "2.0rc1": { "v1-index_bundle.zip": { common: 60 } },
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.classicrummy": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": {} },
          IOS: {
            "v1-strings.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": {} },
            "v1-configuration.zip": {},
            "v1-icons.zip": {},
          },
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.classplus": {
        release: {
          IOS: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.cloverventures": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {} },
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.curefit": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-ui_config.zip": {} },
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.dineout": {
        release: {
          "2.0rc1": { "index_bundle.js": { common: 99 }, "v1-index_bundle.zip": {} },
          IOS: {
            "2.0rc1": { "index_bundle.js": { common: 99 }, "v1-index_bundle.zip": {} },
            "v1-configuration.zip": {},
            "v1-icons.zip": {},
            "v1-strings.zip": {},
          },
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.freshtohome": {
        release: {
          IOS: { "v1-icons.zip": {}, "v1-strings.zip": {}, "v1-configuration.zip": {} },
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.frontrow": {
        release: {
          IOS: {
            "v1-configuration.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": {} },
            "v1-icons.zip": {},
            "v1-strings.zip": {},
          },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.generic": { release: { "v1-ui_config.zip": {} } },
      "in.juspay.hyperpay.geddit": {
        release: {
          "v1-configuration.zip": {},
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {} },
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.gyandairy": {
        release: {
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.healthifyme": {
        release: {
          IOS: {
            "v1-icons.zip": {},
            "v1-strings.zip": {},
            "v1-configuration.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": {} },
          },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-icons.zip": {},
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.icicilombard": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.idea": { release: { "v1-ui_config.zip": {} } },
      "in.juspay.hyperpay.jiosaavn": {
        release: {
          "v1-icons.zip": {},
          IOS: { "v1-ui_config.zip": {} },
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.kapiva": { release: { "v1-strings.zip": {}, "v1-configuration.zip": {} } },
      "in.juspay.hyperpay.kalkifashion": { release: { "v1-strings.zip": {}, IOS: { "v1-strings.zip": {} } } },
      "in.juspay.hyperpay.kiranakart": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {} },
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.lenskart": {
        release: { IOS: { "v1-configuration.zip": {} }, "v1-configuration.zip": {} },
      },
      "in.juspay.hyperpay.meesho": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.lenstable": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } } },
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
        },
      },
      "in.juspay.hyperpay.mamaearth": {
        release: {
          IOS: {
            "v1-configuration.zip": { common: 99 },
            "v1-strings.zip": {},
            "v1-icons.zip": {},
            "2.0rc1": { "v1-index_bundle.zip": {} },
          },
          "v1-strings.zip": {},
          "v1-icons.zip": {},
          "v1-configuration.zip": { common: 99 },
          "2.0rc1": { "v1-index_bundle.zip": {} },
        },
      },
      "in.juspay.hyperpay.meesho1": { release: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } } } },
      "in.juspay.hyperpay.mpokket": {
        release: { IOS: { "v1-icons.zip": {}, "v1-strings.zip": {} }, "v1-strings.zip": {}, "v1-icons.zip": {} },
      },
      "in.juspay.hyperpay.mxplayer": {
        release: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
      },
      "in.juspay.hyperpay.navi": {
        release: {
          IOS: { "v1-configuration.zip": {} },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.okcredit": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-strings.zip": { common: 99 },
          IOS: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": { common: 99 } },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-ui_config.zip": { common: 99 },
        },
      },
      "in.juspay.hyperpay.olacabs": {
        release: {
          IOS: { "v1-icons.zip": {}, "v1-ui_config.zip": {}, "v1-strings.zip": {} },
          "v1-ui_config.zip": {},
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.okcstable": { release: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      "in.juspay.hyperpay.olanew": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.hyperpay.omi": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.onecard": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.physics": {
        release: { "v1-configuration.zip": {}, "v1-strings.zip": {}, "v1-icons.zip": {} },
      },
      "in.juspay.hyperpay.pocketfm": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {}, "v1-icons.zip": {} },
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.provilac": { release: { IOS: { "v1-icons.zip": {} }, "v1-icons.zip": {} } },
      "in.juspay.hyperpay.qmin": {
        release: {
          IOS: {
            "configuration.js": { common: 99 },
            "v1-ui_config.zip": {},
            "v1-strings.zip": {},
            "strings.js": { common: 99 },
          },
          "strings.js": { common: 99 },
          "configuration.js": { common: 99 },
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.sachargamingpp": {
        release: {
          "v1-strings.zip": {},
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": { common: 5 } },
          "v1-configuration.zip": { common: 5 },
        },
      },
      "in.juspay.hyperpay.shoppersstop": {
        release: { IOS: { "v1-configuration.zip": {} }, "v1-configuration.zip": {} },
      },
      "in.juspay.hyperpay.slice": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-configuration.zip": {}, "v1-icons.zip": {} },
          "v1-strings.zip": {},
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.stockgro": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-strings.zip": {} },
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.tagmango": {
        release: {
          IOS: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.tatabinge": {
        release: {
          IOS: { "v1-ui_config.zip": {}, "v1-strings.zip": {} },
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.tatasky": {
        release: {
          IOS: { "v1-strings.zip": {}, "v1-icons.zip": {}, "v1-ui_config.zip": {} },
          "v1-icons.zip": {},
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.test": {
        release: {
          "v1-strings.js": { common: 10 },
          "strings.js": { common: 5 },
          "v1-configuration.jsa": { common: 5 },
          "v1-strings.jsa": { common: 5 },
          "v1-strings.zip": {},
        },
        beta: {
          "v1-ui_config.zip": { tester: 45, newtest: 56, newesttest: 3, abc: 2, common: 26, prtest: 99 },
          "2.0rc1": { "v1-index_bundle.zip": { common: 24, swiggy: 25 } },
        },
      },
      "in.juspay.hyperpay.timesprime": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 }, "v1-v1-index_bundle.zip": {} },
          IOS: {
            "2.0rc1": { "v1-v1-index_bundle.zip": {}, "v1-index_bundle.zip": { common: 99 } },
            "v1-configuration.zip": { common: 99 },
            "v1-ui_config.zip": {},
          },
          "v1-configuration.zip": { common: 99 },
          "v1-ui_config.zip": {},
          "v1-icons.zip": {},
          "v1-strings.zip": { common: 99 },
        },
      },
      "in.juspay.hyperpay.timesprimestable": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.hyperpay.tmc": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} },
          "v1-icons.zip": {},
          "v1-strings.zip": {},
          "v1-configuration.zip": {},
        },
      },
      "in.juspay.hyperpay.toi": {
        release: {
          "v1-configuration.zip": { common: 99 },
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          "v1-configuration2.zip": {},
          "v1-icons.zip": {},
          "v1-icons2.zip": {},
          "v1-strings.zip": {},
          "v1-strings2.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.travolook": {
        release: {
          "v1-configuration.zip": {},
          IOS: { "v1-configuration.zip": {}, "v1-icons.zip": {} },
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.truefan": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-ui_config.zip": {} },
          "v1-configuration.zip": { common: 75 },
          "v1-ui_config.zip": { common: 99 },
          "v1-icons.zip": {},
          "v1-strings.zip": {},
        },
        beta: { "v1-strings.zip": { common: 99 }, "v1-ui_config.zip": { common: 99 } },
      },
      "in.juspay.hyperpay.trulymadly": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.udaan": {
        release: {
          IOS: { "v1-ui_config.zip": {} },
          "icons.js": { common: 99 },
          "icons.zip": { common: 99 },
          "v1-icons.zip": {},
          "v1-ui_config.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.urbanclap": {
        release: {
          IOS: { "v1-configuration.zip": {}, "v1-ui_config.zip": {}, "v1-strings.zip": {} },
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
          "v1-strings.zip": {},
          "v1-ui_config.zip": {},
        },
      },
      "in.juspay.hyperpay.versioningtest": {
        release: {
          IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
        },
      },
      "in.juspay.hyperpay.vistable": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } } },
        },
      },
      "in.juspay.hyperpay.vodafonerebrand": {
        release: { "v1-configuration.zip": {}, IOS: { "v1-configuration.zip": {} } },
      },
      "in.juspay.hyperpay.vodafonetv": {
        release: {
          IOS: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} },
          "v1-configuration.zip": {},
          "v1-icons.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.vodaidea": {
        release: {
          IOS: {
            "2.0rc1": { "v1-index_bundle.zip": {} },
            "v1-ui_config.zip": { common: 99 },
            "v1-icons.zip": { common: 99 },
            "v1-configuration.zip": {},
            "v1-strings.zip": { common: 99 },
          },
          "v1-strings.zip": { common: 99 },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-configuration.zip": { common: 99 },
          "v1-ui_config.zip": { common: 99 },
          "v1-icons.zip": {},
        },
      },
      "in.juspay.hyperpay.wholetruthfoods": { release: { IOS: { "v1-strings.zip": {} }, "v1-strings.zip": {} } },
      "in.juspay.hyperpay.zee5": {
        release: {
          "strings.js": { zee5: 99 },
          "2.0rc1": { "v1-configuration.zip": { zee5: 99 }, "v1-index_bundle.zip": { common: 99 } },
          "v1-ui_config.zip": {},
          "v1-icons.zip": {},
          "configuration.js": { zee5: 99 },
          "v1-configuration.zip": {},
          "v1-strings.zip": {},
        },
      },
      "in.juspay.hyperpay.zee1": { release: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } } } },
      "in.juspay.hyperpay": {
        beta: { "2.0rc1": { "v1-configuration.zip": { slice: 99, croma: 99, qmin: 99 } } },
        release: {
          "2.0rc1": {
            "2.0.1": { "v1-index_bundle.zip": {} },
            "2.0.2": { "v1-index_bundle.zip": { onecard: 99 } },
            "2.0rc1_607": { "index_bundle.js": {} },
            "index_bundle.js": {},
            "v1-index_bundle.zip": { slice: 99 },
          },
          IOS: {
            "2.0rc1": {
              "2.0rc1_607": { "index_bundle.js": {} },
              "v1-index_bundle.zip": { slice: 99 },
              "2.0.1": { "v1-index_bundle.zip": {} },
              "2.0.2": { "v1-index_bundle.zip": { onecard: 99 } },
            },
            "v1-strings.zip": { lenskartdomestic: 99 },
            "v1-icons.zip": { lenskartdomestic: 99 },
            "v1-configuration.zip": { stockgro: 10, vodaidea: 3, lenskartdomestic: 99 },
          },
          "v1-icons.zip": { lenskartdomestic: 99 },
          "v1-configuration.zip": { stockgro: 10, vodaidea: 3, lenskartdomestic: 99 },
          "v1-strings.zip": { lenskartdomestic: 99 },
          "v1-polyfill.zip": {},
        },
      },
      "in.juspay.hyperupi.slice": {
        release: { "2.0rc1": { "v1-index_bundle.zip": {} }, IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      },
      "in.juspay.hyperupi": {
        beta: { "2.0rc1": { "v1-index_bundle.zip": {} }, "v1-config.zip": {} },
        release: { "v1-config.zip": {} },
      },
      "in.juspay.hyperupi.okcredit": { beta: { "2.0rc1": { "v1-index_bundle.zip": {} } } },
      "in.juspay.inappupi.inappupi": { release: { IOS: { "v1-config.zip": {} }, "v1-config.zip": {} } },
      "in.juspay.inappupi.slice": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.inappupi": {
        release: {
          IOS: { "v1-config.zip": {}, "2.0rc1": { "v1-index_bundle.zip": {} } },
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "v1-config.zip": {},
        },
      },
      "in.juspay.upiintent.toi": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.okcstable": {
        release: { "2.0rc1": { "v1-index_bundle.zip": { onecard: 99, okcredit: 50, stockgro: 99 } } },
      },
      "in.juspay.upiintent.frontrow": {
        release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } }, "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay.upiintent": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": { common: 99 } },
          IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 99 } }, "v1-config.zip": { common: 99 } },
          "v1-config.zip": {},
          "config.js": { common: 99 },
        },
        beta: { "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
      "in.juspay": {
        upiintent: { release: { IOS: { "2.0rc1": { "v1-index_bundle.zip": { common: 5 } } } } },
      },
      "in.juspay.vies": {
        release: {
          "2.0rc1": { "v1-index_bundle.zip": {} },
          "config.js": { common: 99 },
          "v1-config.zip": { common: 99 },
          IOS: { "2.0rc1": { "v1-index_bundle.zip": {} } },
        },
        beta: { "2.0rc1": { "v1-index_bundle.zip": {} } },
      },
    },
    "hyper-os": { "in.juspay.hyperos": { release: { "2.0rc1": { "v1-boot_loader.zip": {} } } } },
  },
  hyper: {
    bundles: {
      android: {
        release: {
          "in.juspay.dotp": { "2.0.0": { common: { stable: { "v1-config.zip": {} } } } },
          "in.juspay.ec": {
            "2.0.0": {
              slice: { stable: { "v1-index_bundle.zip": {} } },
              bboutdoor: { stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.flyer": {
            "2.0.0": {
              common: { stable: { "v1-index_bundle.zip": {} } },
              dineout: {
                stable: { "index_bundle.js": { common: 20 } },
                "v1-index_bundle.zip": { common: 45 },
              },
              ixigoprod: { stable: { "v1-index_bundle.zip": {} } },
              timesprime: { stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.hyperpay": {
            "2.0.0": {
              a23games: {
                stable: {
                  "v1-icons.zip": { common: 99 },
                  "v1-strings.zip": {},
                  "v1-index_bundle.zip": { common: 99 },
                  "v1-configuration.zip": {},
                },
              },
              adda52poker: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              acko: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {} } },
              bbfresho: { stable: { "v1-strings.zip": {}, "v1-ui_config.zip": {} } },
              cars24: { stable: { "v1-configuration.zip": {} } },
              bboutdoor: {
                stable: { "v1-ui_config.zip": {} },
                "ui_config.js": {},
                staggered: { "ui_config.js": {} },
              },
              common: { "2.0rc1_607": { "index_bundle.js": {} } },
              bbinstant: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              classicrummy: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              cloverventures: { stable: { "v1-configuration.zip": {} } },
              curefit: { stable: { "v1-ui_config.zip": {}, "v1-strings.zip": {} } },
              citymall: { stable: { "v1-strings.zip": {}, "v1-configuration.zip": { common: 99 } } },
              freshtohome: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              dineout: { stable: { "index_bundle.js": { common: 20 }, "v1-configuration.zip": {} } },
              frontrow: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              geddit: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              gyandairy: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              healthifyme: { stable: { "v1-configuration.zip": {} } },
              icicilombard: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              jiosaavn: { stable: { "v1-ui_config.zip": {} } },
              mpokket: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {} } },
              kalkifashion: { stable: { "v1-strings.zip": {} } },
              mxplayer: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              mamaearth: { stable: { "v1-configuration.zip": {} } },
              lenskart: { stable: { "v1-configuration.zip": {} } },
              meesho1: { stable: { "v1-index_bundle.zip": { common: 99 } } },
              navi: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              onecard: { stable: { "v1-configuration.zip": {} } },
              pocketfm: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              physics: { stable: { "v1-configuration.zip": {} } },
              provilac: { stable: { "v1-strings.zip": {} } },
              qmin: { stable: { "v1-strings.zip": {}, "v1-ui_config.zip": {} } },
              shopprtv: { stable: { "v1-strings.zip": {} } },
              sachargamingpp: { stable: { "v1-configuration.zip": { common: 5 } } },
              stockgro: { stable: { "v1-configuration.zip": {} } },
              slice: { stable: { "v1-strings.zip": {}, "v1-configuration.zip": {} } },
              shoppersstop: { stable: { "v1-configuration.zip": {} } },
              tagmango: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {}, "v1-configuration.zip": {} } },
              timesprime: {
                stable: { "v1-configuration.zip": { common: 99 }, "v1-index_bundle.zip": { common: 99 } },
              },
              tmc: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {}, "v1-icons.zip": {} } },
              tatasky: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-ui_config.zip": {} } },
              toi: {
                stable: {
                  "v1-configuration.zip": { common: 99 },
                  "v1-configuration2.zip": {},
                  "v1-index_bundle.zip": { common: 99 },
                  "v1-strings2.zip": {},
                  "v1-icons2.zip": {},
                },
              },
              travolook: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {} } },
              udaan: { stable: { "v1-icons.zip": {}, "v1-ui_config.zip": {}, "v1-strings.zip": {} } },
              urbanclap: { stable: { "v1-configuration.zip": {}, "v1-ui_config.zip": {}, "v1-strings.zip": {} } },
              vodaidea: {
                stable: { "v1-configuration.zip": {} },
                staggered: { "configuration.js": { vodaidea: 10 } },
              },
              zee1: { stable: { "v1-index_bundle.zip": {} } },
              vodafonerebrand: { stable: { "v1-configuration.zip": {} } },
              vodafonetv: { stable: { "v1-configuration.zip": {} } },
              zee5: { stable: { "v1-index_bundle.zip": {} }, "v1-index_bundle.zip": { common: 50 } },
              wholetruthfoods: { stable: { "v1-strings.zip": {} } },
            },
            "2.0rc1": {
              zee1: { stable: { "v1-index_bundle.zip": {} } },
              cars24: { stable: { "v1-configuration.zip": {} } },
              zee5: { "v1-index_bundle.zip": { common: 50 }, stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.inappupi": {
            "2.0.0": {
              slice: { stable: { "v1-index_bundle.zip": {} } },
              inappupi: { stable: { "v1-config.zip": {} } },
              common: { stable: { "v1-config.zip": {} } },
            },
          },
          "in.juspay.hyperupi": {
            "2.0.0": {
              slice: { stable: { "v1-index_bundle.zip": {} } },
              common: { stable: { "v1-config.zip": {} } },
            },
          },
          "in.juspay.upiintent": {
            "2.0.0": {
              toi: { stable: { "v1-index_bundle.zip": {} } },
              common: { stable: { "v1-index_bundle.zip": {}, "v1-config.zip": {} } },
              frontrow: { stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.vies": { "2.0.0": { common: { stable: { "v1-config.zip": { common: 99 } } } } },
        },
      },
      ios: {
        release: {
          "in.juspay.flyer": {
            "2.0.0": {
              common: { stable: { "v1-index_bundle.zip": {} } },
              ixigoprod: { stable: { "v1-index_bundle.zip": {} } },
              dineout: {
                stable: { "index_bundle.js": { common: 20 } },
                "v1-index_bundle.zip": { common: 45 },
              },
              timesprime: { stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.ec": { "2.0.0": { slice: { stable: { "v1-index_bundle.zip": {} } } } },
          "in.juspay.hyperpay": {
            "2.0.0": {
              a23games: {
                stable: {
                  "v1-configuration.zip": {},
                  "v1-icons.zip": { common: 99 },
                  "v1-index_bundle.zip": { common: 99 },
                  "v1-strings.zip": {},
                },
              },
              acko: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              adda52poker: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {}, "v1-icons.zip": {} } },
              classicrummy: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              cars24: { stable: { "v1-configuration.zip": {} } },
              bbinstant: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              common: { "2.0rc1_607": { "index.js": {}, "index_bundle.js": {} } },
              curefit: { stable: { "v1-ui_config.zip": {}, "v1-strings.zip": {} } },
              cloverventures: { stable: { "v1-configuration.zip": {} } },
              dineout: { stable: { "index_bundle.js": { common: 20 }, "v1-configuration.zip": {} } },
              freshtohome: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              frontrow: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {}, "v1-configuration.zip": {} } },
              gyandairy: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              icicilombard: { stable: { "v1-icons.zip": {}, "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              kalkifashion: { stable: { "v1-strings.zip": {} } },
              lenskart: { stable: { "v1-configuration.zip": {} } },
              jiosaavn: { stable: { "v1-ui_config.zip": {} } },
              healthifyme: { stable: { "v1-configuration.zip": {} } },
              onecard: { stable: { "v1-configuration.zip": {} } },
              geddit: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              mpokket: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {} } },
              mamaearth: { stable: { "v1-configuration.zip": {} } },
              pocketfm: { stable: { "v1-strings.zip": {}, "v1-configuration.zip": {}, "v1-icons.zip": {} } },
              navi: { stable: { "v1-strings.zip": {} } },
              shoppersstop: { stable: { "v1-configuration.zip": {} } },
              slice: { stable: { "v1-configuration.zip": {}, "v1-strings.zip": {} } },
              sachargamingpp: { stable: { "v1-configuration.zip": { common: 5 } } },
              provilac: { stable: { "v1-strings.zip": {} } },
              stockgro: { stable: { "v1-configuration.zip": {} } },
              tatasky: { stable: { "v1-icons.zip": {}, "v1-ui_config.zip": {} } },
              tagmango: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {}, "v1-configuration.zip": {} } },
              qmin: { stable: { "v1-strings.zip": {} } },
              timesprime: {
                stable: { "v1-configuration.zip": { common: 99 }, "v1-index_bundle.zip": { common: 99 } },
              },
              tmc: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {}, "v1-strings.zip": {} } },
              travolook: { stable: { "v1-configuration.zip": {}, "v1-icons.zip": {} } },
              vodafonetv: { stable: { "v1-configuration.zip": {} } },
              vodafonerebrand: { stable: { "v1-configuration.zip": {} } },
              urbanclap: { stable: { "v1-configuration.zip": {}, "v1-ui_config.zip": {}, "v1-strings.zip": {} } },
              vodaidea: { stable: { "v1-configuration.zip": {} } },
              wholetruthfoods: { stable: { "v1-strings.zip": {} } },
            },
            "2.0rc1": { cars24: { stable: { "v1-configuration.zip": {} } } },
          },
          "in.juspay.hyperupi": { "2.0.0": { slice: { stable: { "v1-index_bundle.zip": {} } } } },
          "in.juspay.upiintent": {
            "2.0.0": {
              common: { stable: { "v1-index_bundle.zip": {} } },
              toi: { stable: { "v1-index_bundle.zip": {} } },
              frontrow: { stable: { "v1-index_bundle.zip": {} } },
            },
          },
          "in.juspay.inappupi": {
            "2.0.0": {
              inappupi: { stable: { "v1-config.zip": {} } },
              slice: { stable: { "v1-index_bundle.zip": { common: 5 } } },
              common: { stable: { "v1-config.zip": {} } },
            },
          },
        },
      },
      web: {
        beta: {
          "in.juspay.hyperpay": {
            "2.0.0": {
              common: { "2.0.3": { "index.js": { picasso: 99 } } },
              picasso: { stable: { "configuration.js": {} } },
              trell: { stable: { "configuration.js": { common: 50 } } },
            },
          },
        },
        release: {
          "in.juspay.ec": { "2.0.0": { common: { stable: { "index.js": {} } } } },
          "in.juspay.flyer": {
            "2.0.0": {
              common: { stable: { "index.js": {} } },
              timesprime: { stable: { "index.js": {} } },
              ixigoprod: { stable: { "index.js": {} } },
            },
          },
          "in.juspay.dotp": { "2.0.0": { common: { stable: { "config.js": {} } } } },
          "in.juspay.hyperpay": {
            "2.0.0": {
              classicrummy: {
                stable: {
                  "configuration.js": {},
                  "v1-configuration.zip": {},
                  "strings.js": {},
                  "v1-strings.zip": { common: 15 },
                },
              },
              acko: { stable: { "strings.js": {}, "icons.js": {}, "configuration.js": {} } },
              astroyogi: { stable: { "strings.js": { common: 99 }, "index.js": { common: 5 } } },
              coda: { stable: { "configuration.js": {} } },
              collegedekho: { stable: { "configuration.js": {}, "strings.js": {} } },
              common: { "2.0rc1_607": { "index.js": {} }, "2.0.3": { "index.js": { picasso: 99 } } },
              dineout: { stable: { "configuration.js": {} } },
              fastandup: { stable: { "configuration.js": {} } },
              fooddarzee: { stable: { "configuration.js": {} } },
              geddit: {
                stable: {
                  "configuration.js": {},
                  "v1-strings.zip": {},
                  "v1-configuration.zip": {},
                  "strings.js": {},
                },
              },
              icicilombard: { stable: { "configuration.js": {}, "strings.js": {}, "icons.js": {} } },
              frontrow: { stable: { "configuration.js": {}, "v1-configuration.zip": {} } },
              healthifyme: { stable: { "configuration.js": {} } },
              ixigoprod: { stable: { "index.js": {}, "configuration.js": {}, "icons.js": {}, "strings.js": {} } },
              ixigo: { stable: { "index.js": { common: 99 } } },
              kapiva: { stable: { "strings.js": {}, "configuration.js": {} } },
              lenskart: { stable: { "configuration.js": {} } },
              lenskartdomestic: { stable: { "icons.js": {}, "strings.js": {}, "configuration.js": {} } },
              mamaearthacc: {
                stable: { "configuration.js": {}, "strings.js": {}, "icons.js": {}, "index.js": {} },
              },
              mamaearth: { stable: { "v1-configuration.zip": {}, "configuration.js": {} } },
              mpokket: { stable: { "v1-icons.zip": {}, "v1-strings.zip": {} } },
              lidolearning: { stable: { "strings.js": {}, "configuration.js": {} } },
              myscoot: { stable: { "v1-configuration.zip": { common: 45 }, "configuration.js": {} } },
              mysleepyhead: { stable: { "configuration.js": {} } },
              nuawellness: { stable: { "configuration.js": {}, "strings.js": {} } },
              picasso: {
                cug: { "configuration.js": { picasso: 99 } },
                stable: { "strings.js": { common: 10 } },
              },
              navi: { stable: { "strings.js": {} } },
              provilac: { stable: { "strings.js": {}, "icons.js": {} } },
              pocketfm: { stable: { "configuration.js": {}, "icons.js": {} } },
              qmin: { stable: { "strings.js": {} } },
              physics: { stable: { "configuration.js": {} } },
              tagmango: { stable: { "configuration.js": {}, "strings.js": {}, "icons.js": {} } },
              slice: {
                stable: {
                  "configuration.js": {},
                  "strings.js": {},
                  "v1-configuration.zip": {},
                  "v1-strings.zip": {},
                },
              },
              stockgro: {
                staggered: { "strings.js": { common: 99 } },
                stable: { "configuration.js": {}, "strings.js": {} },
              },
              timesprime: { stable: { "index.js": {}, "configuration.js": {} } },
              tatabinge: { stable: { "ui_config.js": { common: 99 }, "icons.js": {}, "configuration.js": {} } },
              toi: {
                stable: {
                  "configuration2.js": {},
                  "strings2.js": {},
                  "index.js": {},
                  "configuration.js": {},
                  "icons2.js": {},
                  "icons.js": {},
                  "strings.js": {},
                },
              },
              zee5: { stable: { "configuration.js": {}, "index.js": {} } },
              travolook: {
                stable: { "configuration.js": {}, "index.js": { collegedekho: 99, common: 99 }, "icons.js": {} },
              },
              vodaidea: { stable: { "configuration.js": {} } },
              trell: { stable: { "configuration.js": { common: 10 } } },
              udaan: { stable: { "strings.js": {}, "icons.js": {} } },
            },
          },
          "in.juspay.hypercredit": {
            "2.0.0": {
              vedantu: { stable: { "icons.js": {}, "configuration.js": {}, "strings.js": {} } },
              picasso: { stable: { "configuration.js": {} } },
              twinhealth: { stable: { "configuration.js": {} } },
              common: { stable: { "index.js": {} } },
            },
          },
          "in.juspay.hyperos": { "2.0.0": { "config.js": {}, common: { stable: { "tracker.js": {} } } } },
          "in.juspay.upiintent": {
            "2.0.0": {
              frontrow: { stable: { "index.js": {} } },
              common: { stable: { "index.js": {}, "config.js": {} } },
              toi: { stable: { "index.js": {} } },
            },
          },
          "in.juspay.inappupi": { "2.0.0": { common: { stable: { "config.js": {} } } } },
        },
      },
      release: {
        "in.juspay.hyperpay": { android: { "2.0rc1": { zee5: { stagger: { "v1-index_bundle.zip": {} } } } } },
      },
    },
  },
};

var highStaggerAssets = {};

const findAsset = function (keyPath, staggerData) {
  for (var key in staggerData) {
    if (typeof staggerData[key] == "number") {
      if (staggerData[key] > 95) {
        highStaggerAssets[keyPath + "/" + key] = staggerData[key];
      }
    } else {
      findAsset(keyPath + "/" + key, staggerData[key]);
    }
  }
};

findAsset("", staggerData);
console.log(highStaggerAssets);
